# Fiches Créatures

Création de fiches créatures au format carte Magic.

Ces fiches contiennent les éléments nécessaires pour gérer 2 cas au moins :
- Fiche de créature annexe (convocation, familier)
- Transmutation (Stats mixées entre le perso et la créature cible)

Les informations contenues sont : 
- caractéristiques
- Infos créature (type, taille, portée)
- Capacités spéciales
- Illustration

Actuellement toutes les données doivent être choisies et remplies manuellement  
(Le format de saisie n'est pas encore défini, il y aura sûrement un fichier de  
template à télécharger, modifier et renvoyer).