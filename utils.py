import unicodedata
import string
from PIL import Image

valid_filename_chars = "-_.() %s%s" % (string.ascii_letters, string.digits)
char_limit = 255

def clean_filename(filename, whitelist=valid_filename_chars, replace=' '):
    # replace spaces
    for r in replace:
        filename = filename.replace(r,'_')
    
    # keep only valid ascii chars
    cleaned_filename = unicodedata.normalize('NFKD', filename).encode('ASCII', 'ignore').decode()
    
    # keep only whitelisted chars
    cleaned_filename = ''.join(c for c in cleaned_filename if c in whitelist)
    if len(cleaned_filename)>char_limit:
        print("Warning, filename truncated because it was over {}. Filenames may no longer be unique".format(char_limit))
    return cleaned_filename[:char_limit]

# Function to fit logo size for fg in the background)
def prepareLogoSize(logo, bg):
    # Calcul taille du cadre
    maxWidth  = int(bg.size[0] * 0.66)
    maxHeight = int(bg.size[1] * 0.45)
    #print(maxWidth, maxHeight)

    # Check format portait ou paysage pour calcul taille cible
    if logo.size[0] > logo.size[1]:
        print("paysage")
        w = maxWidth
        h = int((w / logo.size[0]) * logo.size[1])
        startY = int((maxHeight - h) / 2)

        # Ajout décalages x et y pour place carac et bord
        #print(bg.size[0] * 0.24)
        startX = int(bg.size[0] * 0.24)
        startY = int(startY + bg.size[1] * 0.02)
    else:
        print("portrait")
        h = maxHeight
        w = int((h / logo.size[1]) * logo.size[0])
        startX = int((maxWidth - w) / 2)

        # Ajout décalages x et y pour place carac et bord
        #print(bg.size[0] * 0.24)
        startX = int(startX + bg.size[0] * 0.24)
        startY = int(bg.size[1] * 0.02)

    print(w, h, startX, startY)
    logo = logo.resize((w, h))
    return logo, startX, startY
