"""génération de l'image composite de créature depuis ses stats et logo """
# import os
from PIL import Image, ImageDraw, ImageFont
from blend_modes import multiply
import numpy
import utils


def creer_carte_img(title, sous_type, source, size, reach, points_vie,
                    carac_phy, carac_mental, jets_svg):
    """creation de l'image avec export dans ./output"""

    # -- Ajout de l'illustration
    bgroud = Image.open('./images/FondCreature.png').convert("RGBA")
    logo_name = utils.clean_filename(title + '_logo.png')
    logo = Image.open('./images/' + logo_name).convert("RGBA")

    logo, start_x, start_y = utils.prepareLogoSize(logo, bgroud)

    # Création du masque avec logo à la bonne taille + décalage
    fground = Image.new('RGBA', bgroud.size)
    fground.paste(logo, (start_x, start_y))

    bg_array = numpy.array(bgroud).astype(float)
    fg_array = numpy.array(fground).astype(float)

    blended_array = multiply(bg_array, fg_array, 1.0)

    img = Image.fromarray(numpy.uint8(blended_array))
    # img.show()

    # -- Préparation pour textes
    font30 = ImageFont.truetype("fonts/blackchancery.regular.ttf", 30)
    font18 = ImageFont.truetype("fonts/blackchancery.regular.ttf", 18)
    font15 = ImageFont.truetype("fonts/blackchancery.regular.ttf", 15)
    font13 = ImageFont.truetype("fonts/blackchancery.regular.ttf", 13)
    symbol15 = ImageFont.truetype("fonts/Lato-Bold.ttf", 15)

    # Edition de l'image
    drawing = ImageDraw.Draw(img)

    # Ajout Title
    drawing.text((30, 20), title, font=font18, fill=(0, 0, 0))
    # Ajout Type
    drawing.text((30, 298), sous_type, font=font18, fill=(0, 0, 0))
    # Ajout Size
    drawing.text((235, 301), size, font=font15, fill=(0, 0, 0))
    # Ajout Reach
    drawing.text((330, 301), reach, font=font15, fill=(0, 0, 0))
    # Ajout Source
    drawing.text((85, 497), source, font=font13, fill=(255, 255, 255))
    # Ajout PV
    drawing.text((35, 475), points_vie, font=font30, fill=(255, 255, 255))
    # Ajout carac physiques
    force = carac_phy[0]
    dex = carac_phy[1]
    constit = carac_phy[2]
    drawing.text((75, 45), force, font=font18, fill=(0, 0, 0))
    drawing.text((71, 64), dex, font=font18, fill=(0, 0, 0))
    drawing.text((67, 78), constit, font=font18, fill=(0, 0, 0))
    # Ajout carac mentales
    intel = carac_mental[0]
    sagesse = carac_mental[1]
    charisme = carac_mental[2]
    drawing.text((65, 98), intel, font=font18, fill=(0, 0, 0))
    drawing.text((65, 113), sagesse, font=font18, fill=(0, 0, 0))
    drawing.text((66, 131), charisme, font=font18, fill=(0, 0, 0))
    # Ajout JS
    js_prefix_for = jets_svg[0][0]
    js_prefix_ref = jets_svg[1][0]
    js_prefix_vol = jets_svg[2][0]
    js_for = jets_svg[0][1:]
    js_ref = jets_svg[1][1:]
    js_vol = jets_svg[2][1:]
    drawing.text((64, 170), js_prefix_for, font=symbol15, fill=(0, 0, 0))
    drawing.text((74, 170), js_for, font=font18, fill=(0, 0, 0))
    drawing.text((68, 184), js_prefix_ref, font=symbol15, fill=(0, 0, 0))
    drawing.text((78, 184), js_ref, font=font18, fill=(0, 0, 0))
    drawing.text((73, 201), js_prefix_vol, font=symbol15, fill=(0, 0, 0))
    drawing.text((83, 201), js_vol, font=font18, fill=(0, 0, 0))
    # img.show()

    # Sauvegarde du fichier final
    nom_fichier = utils.clean_filename(title + '.png')
    img.save('output/' + nom_fichier)


creer_carte_img("Dragon Faery", "Small Dragon", "Dr", "5x5", "5", "140",
                ["13", "18", "14"], ["16", "31", "20"], ["+15", "+13", "+18"])
# creer_image_carte()
# creercarte_imge("Hydre à 6 têtes", "Créature magique",
# "M1",carte_img10", "57",
# ["17", "12", "20"], ["18", "15", "15"], ["+8", "+3", "+9"])
